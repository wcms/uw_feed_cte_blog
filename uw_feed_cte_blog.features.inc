<?php

/**
 * @file
 * uw_feed_cte_blog.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_feed_cte_blog_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_feed_cte_blog_views_api() {
  return array("version" => "3.0");
}
