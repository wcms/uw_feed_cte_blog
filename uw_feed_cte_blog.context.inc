<?php

/**
 * @file
 * uw_feed_cte_blog.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_feed_cte_blog_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'cte_feed_blog_block';
  $context->description = 'Displays feed blog from feed aggregator';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-uw_cte_feed_blog-cte_block_1' => array(
          'module' => 'views',
          'delta' => 'uw_cte_feed_blog-cte_block_1',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays feed blog from feed aggregator');
  $export['cte_feed_blog_block'] = $context;

  return $export;
}
